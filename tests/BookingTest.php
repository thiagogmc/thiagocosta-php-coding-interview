<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;
use Src\models\ClientModel;

class BookingTest extends TestCase {

	private $booking;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->booking = new Booking();
	}

	/** @test */
	public function getBookings() {
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['clientid'], 1);
		$this->assertEquals($results[0]['price'], 200);
		$this->assertEquals($results[0]['checkindate'], '2021-08-04 15:00:00');
		$this->assertEquals($results[0]['checkoutdate'], '2021-08-11 15:00:00');
	}

    /** @test */
    public function createBooking() {
        $client = [
            'username' => 'newbookinguser',
            'name' => 'New User',
            'email' => 'newuser@dogeplace.com',
            'phone' => '5555555'
        ];

        $bookingData = [
            'price' => 200,
            'checkindate' => '2021-08-04 15:00:00',
            'checkoutdate' => '2021-08-11 15:00:00'
        ];

        $result = $this->booking->createBooking($client, $bookingData);
        $expected = $this->booking->getBookings();
        $expected = end($expected);

        $this->assertEquals($expected['price'], $result['price']);
        $this->assertEquals($expected['clientid'], $result['clientid']);
        $this->assertEquals($expected['price'], $result['price']);
        $this->assertEquals($expected['checkindate'], $result['checkindate']);
        $this->assertEquals($expected['checkoutdate'], $result['checkoutdate']);
    }
}