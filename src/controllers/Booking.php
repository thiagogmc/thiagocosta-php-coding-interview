<?php

namespace Src\controllers;

use Src\models\BookingModel;
use Src\models\ClientModel;
use Src\models\DogModel;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

    private function getClientModel(): ClientModel {
        return new ClientModel();
    }

    private function getDogModel(): DogModel {
        return new DogModel();
    }

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

    public function createBooking(array $client, $bookingData) {
        $client = $this->getClientModel()->findOrCreate($client);
        $bookingData['clientid'] = $client['id'];
        $dogsAverageAge = $this->getDogModel()->getDogsAverageAgeByClientId($bookingData['clientid']);

        if ($dogsAverageAge < 10) {
            $bookingData['price'] = $bookingData['price'] * 0.9;
        }

        return $this->getBookingModel()->createBooking($bookingData);
    }
}