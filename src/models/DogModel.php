<?php

namespace Src\models;

use Src\helpers\Helpers;

class DogModel {

	private $dogData;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/dogs.json');
		$this->dogData = json_decode($string, true);
	}

	public function getDogs() {
		return $this->dogData;
	}

    public function getDogsByClientId(int $id) {
        $dogs = $this->getDogs();
        $dogsArray = [];
        foreach ($dogs as $dog) {
            if ($dog['clientid'] == $id) {
                $dogsArray[] = $dog;
            }
        }
        return $dogsArray;
    }

    public function getDogsAverageAgeByClientId(int $id) {
        $dogs = $this->getDogs();
        $dogsSum = 0;
        $dogsCount = 0;
        foreach ($dogs as $dog) {
            if ($dog['clientid'] == $id) {
                $dogsSum += $dog['age'];
                $dogsCount += 1;
            }
        }

        if ($dogsCount === 0) {
            return 0;
        }

        return $dogsSum / $dogsCount;
    }
}